#coding=utf-8
import bs4
import urllib.request as urllib2
data = []
def load_page(url):
    response = urllib2.urlopen(url)
    return response.read()
def download_page(url,filename):
    filename=filename[1:]
    content = load_page(url)
    import  os
    base_dir = os.path.join(os.getcwd(),os.path.dirname(filename))
    # ''.rfind(r'/')
    _filename=filename[filename.rfind('/')+1:]
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)
    # print os.path.join(base_dir,_filename)
    with open(os.path.join(base_dir,_filename),'wb') as fw:
        fw.write(content)

def main(url):

    raw_content = load_page(url)
    soup = bs4.BeautifulSoup(raw_content,"html.parser")
    categorys = soup.select('li.items > a')
    for category in categorys:
        category_name = category.text
        data.append({'category':category_name,'goods':[]})
        _href = url+category.get('href')
        items_content = load_page(_href)
        parse_goods(items_content, url,category_name)


def _find(category_name):
    for d in data:
        if d['category']==category_name:
            return d['goods']


def parse_goods(items_content, url,category_name):
    items_soup = bs4.BeautifulSoup(items_content, 'html.parser')
    _goods = items_soup.select('div.good')
    for goods in _goods:
        goods_name = goods.find('p', attrs={'class': 'name'}).text
        goods_price = goods.find('p', attrs={'class': 'big'}).text
        goods_oldprice = goods.find('s').text[1:]
        _details = url + goods.find('a').get('href')
        goods_desc, colors, sizes, specs=parse_details(_details, url)
        goods = {'goodsname':goods_name,'goods_price':goods_price,'goods_oldprice':goods_oldprice,'goods_desc':goods_desc,'colors':colors,'sizes':sizes,'specs':specs}
        _find(category_name).append(goods)
    _next = items_soup.find(id='pager').find('a')
    if _next !=None and _next.text.strip() == u'下一页':
         next_page =url+ items_soup.find(id='pager').find('a').get('href')
         # print u'下一页-'+next_page
         parse_goods(load_page(next_page),url,category_name)

def parse_details(_details, url):
    _details_content = load_page(_details)
    details_soup = bs4.BeautifulSoup(_details_content, 'html.parser')
    _content_right = details_soup.find('div', attrs={'class': 'detail-con-right'})
    goods_desc = _content_right.find('h3').text
    _colors = _content_right.find('div', attrs={'class', 'color'})
    colors = parse_color(_colors, url)
    _sizes = _content_right.select('li.size-item')
    sizes = parse_size(_sizes)
    _spes = details_soup.select('.zhanshi-model')
    specs = parse_spec(_spes, url)
    return goods_desc,colors,sizes,specs


def parse_color(_colors, url):
    _data=[]
    colors = _colors.select('img')
    for color in colors:
        color_name = color.get('name')
        color_value = color.get('src')
        _color_url = url + color_value
        download_page(_color_url, color_value)
        _data.append((color_name,color_value))
    return _data

def parse_size(_sizes):
    return [(size.text,size.text) for size in _sizes]


def parse_spec(_spes, url):
    _data=[]
    for spe in _spes:
        spe_desc = spe.find('p').text
        spe_imgs = [_img.get('src') for _img in spe.select('img')]
        _data.append((spe_desc,spe_imgs))
        [download_page(url + img, img) for img in spe_imgs]
    return _data

def tojsonfile(data,filename):
    import json
    jsondata = json.dumps(data)
    with open(filename,'w') as fw:
        fw.write(jsondata)

if __name__ == '__main__':
    url = 'http://127.0.0.1:8000'
    main(url)
    tojsonfile(data,'jiukuaijiu.json')

