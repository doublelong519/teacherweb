class MuitlObjectReturned():
    per_page = 12
    objects = None
    objects_name = 'objects'
    def get_objects(self,page_num = '1'):
        from  django.core.paginator import  Paginator
        # self.objects:获取所有的数据，per_page:每一页的数据
        paginator = Paginator(self.objects,self.per_page)
        # page_num:对象页面的页码，paginator.num_pages  对象一共可以分为几页
        page_num = int(page_num)
        if page_num < 1 :
            page_num = 1
        if page_num > paginator.num_pages :
            page_num = paginator.num_pages
        # 取页码为page_num的对象
        page = paginator.page(page_num)
        # page:当前页面的对象  xrange对象页可迭代的范围，pageinator.object_list:当前页面元素列表
        return {'page':page,'page_range':paginator.page_range,self.objects_name:page.object_list}