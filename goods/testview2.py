# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from goods.models import *
from django.shortcuts import render
from django.views import View
# Create your views here.
class BaseView(View):
    template_name = None
    def get(self,request):
        return render(request,self.template_name,self.get_context(request))
    def get_context(self,request):
        self.context={}
        self.context.update(self.get_extra_context(request))
        return self.context
    def get_extra_context(self,reqeust):
        pass
class GoodsList(BaseView):
    template_name = 'index.html'
    object_name ='goods'
    category_odjects = Category.objects.all()

    def get_extra_context(self,reqeust):
        context={}
        self.category_id = self.request.GET.get('category',Category.objects.first().id)
        context['categorys'] = Category.objects.all()
        context['goods'] = Category.objects.get(id=self.category_id).goods_set.all()
        context['category_id'] = self.category_id
        context['colors'] = Color.objects.values()
        print (context)
        return context
