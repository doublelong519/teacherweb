from django.shortcuts import render

# Create your views here.

from  view.views import BaseView
from  utils.pageutils import MuitlObjectReturned
from goods.models import *
class GoodsListView(BaseView,MuitlObjectReturned):
    # 所有不变的东西，都放到了类的成员当中
    template_name = 'index.html'
    objects_name = 'goods'
    category_objects = Category.objects.all()

    def prepare(self, request):
        # 得到当前类别的id,默认的是类别的第一个的id
        category_id = int(request.GET.get('category', Category.objects.first().id))
        # 根据商品的类别得到商品的信息
        self.objects = Category.objects.get(id=category_id).goods_set.all()
        self.category_id = category_id

    def get_extra_context(self, request):
        # 获取当前页面的页码
        page_num = request.GET.get('page', 1)
        # 跟新数据，重新渲染
        context = {'category_id': self.category_id, 'categorys': self.category_objects}
        context.update(self.get_objects(page_num))
        return context